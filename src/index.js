import React from 'react';
import ReactDOM from 'react-dom';

import SearchContainer from './containers/Search';

const Index =() => {
  return (
    <SearchContainer />
  );
}

ReactDOM.render(<Index />, document.getElementById('index'));
